<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 虚拟人数管理
 * @author chaituan@126.com
 */
class TourismVirtual_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_virtual';
	}
}