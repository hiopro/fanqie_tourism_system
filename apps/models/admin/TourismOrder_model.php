<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 旅游订单
 * @author chaituan@126.com
 */
class TourismOrder_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_order';
	}
}