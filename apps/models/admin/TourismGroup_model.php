<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 旅游分组
 * @author chaituan@126.com
 */
class TourismGroup_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_group';
	}
	
	function cache() {
		$items = $this->getItems();
		set_Cache('tourism_group',$items);
	}
}