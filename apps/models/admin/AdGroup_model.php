<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 广告分类
 * @author chaituan@126.com
 */
class AdGroup_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'ad_group';
	}
	
	function cache() {
		$items = $this->getItems();
		set_Cache('ad_group',$items);
	}
}