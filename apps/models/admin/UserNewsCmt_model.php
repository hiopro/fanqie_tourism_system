<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 用户文章评论管理
 * @author chaituan@126.com
 */
class UserNewsCmt_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'user_news_cmt';
	}
}