<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 旅游配件
 * @author chaituan@126.com
 */
class TourismGoods_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_goods';
	}
	
	function cache(){
		$items = $this->getItems();
		set_Cache('tourism_goods', $items);
	}
}	