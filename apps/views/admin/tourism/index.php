<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
						<div class="layui-input-inline">
							<select  id='day'>
								<option value="">活动天数</option>
								<?php foreach ($day_num as $v){?>
								<option value="<?php echo $v;?>"><?php echo $v > 9?'10 天以上':$v.' 天';?></option>
								<?php }?>
				        	</select>
				        </div>
						<div class="layui-input-inline">
							<input type="text" id="srk"  placeholder="请输入标题" class="layui-input" >
						</div>
				    	<div class="layui-input-inline">
					    <?php echo admin_btn('', 'find',"",'lay-filter="order-find"')?>
					    </div>
					</form>
				</div>
				<div class="layui-inline f-right">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url('adminct/tourism_child/add/id-{{d.id}}-m-58'),'','layui-btn-xs','','添加子活动');?>
<?php echo admin_btn(site_url($dr_url.'/edit/id-{{d.id}}'),'edit','layui-btn-xs');?>
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="sth">
<input type="checkbox" lay-text='显示|关闭' lay-skin="switch" lay-filter='open' {{# if(d.tj==1){ }} checked {{#  } }}   data-url="<?php echo site_url($dr_url.'/tj/id-{{d.id}}')?>" >
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'sort', title: '排序', width: 80,edit:'text'},
	       {field: 'title', title: '标题',edit:'text'},
	       {field: 'money', title: '费用',edit:'text'},
	       {field: 'day_num', title: '活动天数'},
	       {field: 'tj', title: '首页推荐',toolbar: '#sth',width: 90},
	       {field: 'right', title: '操作',toolbar:'#operation', width: 220}
	       ]],
	limit: 15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
	}
});
layui.table.on('edit(common)', function(obj){
    var data = {id:obj.data.id},key = "data["+obj.field+"]";
    	data[key] = obj.value;
    $.post('<?php echo site_url("$dr_url/sort")?>',data,function(d){layer.msg(d.message)},'json');
  });
layui.form.on('submit(order-find)',function(){
	layui.table.reload('common',{//这里的find 是为了后台数据处理
		where:{num:$('#day').val(),srk:$('#srk').val(),find:'find',total:''},
		done:function(res, curr, count){
			this.where.total = count;
			this.where.find = '';
		}
	});
	return false;
});
</script>
<?php echo template('admin/footer');?>