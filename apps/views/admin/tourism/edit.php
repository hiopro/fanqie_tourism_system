<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
				<form class="layui-form  a-e-form" method="post">
						<div class="layui-tab layui-tab-brief" >
						  <ul class="layui-tab-title">
						    <li class="layui-this">基础设置</li>
						    <li>活动内容</li>
						    <li>活动分享</li>
						    <li>活动积分</li>
						    <li>活动佣金</li>
						    <li>相关商品</li>
						  </ul>
						  <div class="layui-tab-content">
						    <div class="layui-tab-item layui-show">
						    	<div class="layui-form-item">
									<label class="layui-form-label">定位城市</label>
									<div class="layui-input-block">
										<select name="data[lid]" >
											<option value="0">此活动不定位</option>
							            	<?php foreach ($location as $v){?>
							            	<option value="<?php echo $v['id'];?>" <?php echo $v['id']==$item['lid']?'selected':''?>  ><?php echo $v['aname'];?></option>
							            	<?php }?>
							        	</select>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动分类</label>
									<div class="layui-input-block">
										<select name="data[tgid]" >
							            	<?php foreach ($group as $v){?>
							            	<option value="<?php echo $v['id'];?>" <?php echo $v['id']==$item['tgid']?'selected':''?>><?php echo $v['tname'];?></option>
							            	<?php }?>
							        	</select>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">集合点</label>
									<div class="layui-input-block">
										<input type="text" name="data[venue]" class="layui-input" lay-verify="required" value="<?php echo $item['venue'];?>">
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动标题</label>
									<div class="layui-input-block">
										<input type="text" name="data[title]" class="layui-input" value="<?php echo $item['title'];?>" lay-verify="required" >
									</div>
								</div>
								<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">活动费用</label>
										<div class="layui-input-inline">
											<input type="text" name="data[money]" class="layui-input" value="<?php echo $item['money'];?>" lay-verify="required" >
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">活动难度</label>
										<div class="layui-input-inline">
											<select name="data[diff]" >
								            	<?php foreach ($diff as $k=>$v){?>
									            	<option value="<?php echo $k;?>"<?php echo $k==$item['diff']?'selected':''?>><?php echo $v."({$k}星)";?></option>
									            <?php }?>
								        	</select>
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">活动天数</label>
										<div class="layui-input-inline">
											<input type="text" name="data[day_num]" class="layui-input" value="<?php echo $item['day_num'];?>" lay-verify="required|number" >
										</div>
									</div>
								</div>
								
								<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">上线人数</label>
										<div class="layui-input-inline" >
											<input type="text" name="data[line_num]" class="layui-input" value="<?php echo $item['line_num'];?>" placeholder="主要针对会员发起活动" lay-verify="required|number" >
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">成行人数</label>
										<div class="layui-input-inline" >
											<input type="text" name="data[in_line]" class="layui-input"  value="<?php echo $item['in_line'];?>" placeholder="预设成行人数，子活动可修改" lay-verify="required|number" >
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">满员人数</label>
										<div class="layui-input-inline" >
											<input type="text" name="data[full]" class="layui-input"  value="<?php echo $item['full'];?>" placeholder="预设满员人数，子活动可修改" lay-verify="required|number" >
										</div>
									</div>
								</div>
								
								<div class="layui-form-item">
									<label class="layui-form-label">活动图片</label>
									<div class="layui-input-block">
										<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px</span>
										<div class="layui-upload layui-hide">
										  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
										  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
									  	</div>
									  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify='thumb' value="<?php echo $item['thumb'];?>" />
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">会员发起</label>
									<div class="layui-input-block">
										<input type="hidden" name="data[is_user]" value="0">
										<input type="checkbox" name="data[is_user]" lay-skin="switch" lay-text="开启|关闭" value="1" <?php echo $item['is_user']?'checked':'';?> > 
									</div>
									<div class="layui-form-mid layui-word-aux">关闭后，不会在自由行中出现</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<div class="layui-form-item">
									<label class="layui-form-label">活动详情</label>
									<div class="layui-input-block">
										<script type="text/plain" id="emditor1" style="width: 100%; min-height: 500px;" name="data[content]"><?php echo $item['content']; ?></script>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">活动行程</label>
									<div class="layui-input-block">
										<script type="text/plain" id="emditor2" style="width: 100%; min-height: 500px;" name="data[journey]"><?php echo $item['journey']; ?></script>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">费用说明</label>
									<div class="layui-input-block">
										<script type="text/plain" id="emditor3" style="width: 100%; min-height: 500px;" name="data[money_say]"><?php echo $item['money_say']; ?></script>
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<div class="layui-form-item">
									<label class="layui-form-label">分享标题</label>
									<div class="layui-input-block">
										<input type="text" name="data[s_title]" class="layui-input" placeholder="不填写默认显示活动标题" value="<?php echo $item['s_title']; ?>" >
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">分享描述</label>
									<div class="layui-input-block">
										<input type="text" name="data[s_desc]" class="layui-input" value="<?php echo $item['s_desc']; ?>" placeholder="不填写默认显示活动标题" >
									</div>
								</div>
						    	<div class="layui-form-item">
									<label class="layui-form-label">分享图片</label>
									<div class="layui-input-block">
										<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px</span>
										<div class="layui-upload layui-hide">
										  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
										  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
									  	</div>
									  	<input name="data[s_thumb]" class="thumb" type="hidden" value="<?php echo $item['s_thumb']; ?>"/>
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<blockquote class="layui-elem-quote">1 取消积分：是用户取消活动扣除的积分。2 分享次数：例如每天分享2次，即可获取2次相应积分。3 积分兑换：填写0.01 为1积分兑换1分RMB</blockquote>
						    	<div class="layui-form-item">
									<div class="layui-inline">
										<label class="layui-form-label">参与积分</label>
										<div class="layui-input-inline">
											<input type="text" name="data[integral_plus]" class="layui-input" value="<?php echo $item['integral_plus']; ?>" >
										</div>
									</div>
									<div class="layui-inline">
										<label class="layui-form-label">取消积分</label>
										<div class="layui-input-inline">
											<input type="text" name="data[integral_subtract]" class="layui-input" value="<?php echo $item['integral_subtract']; ?>" >
										</div>
									</div>
								</div>
						    	<div class="layui-form-item">
									<label class="layui-form-label">一积分兑换</label>
									<div class="layui-input-block">
										<input type="text" name="data[exchange]" class="layui-input" value="<?php echo $item['exchange'];?>" >
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<div class="layui-form-item">
									<label class="layui-form-label">佣金</label>
									<div class="layui-input-block">
										<input type="text" name="data[fx_money]" class="layui-input" placeholder="写 1 代表一块钱" value="<?php echo $item['fx_money'];?>">
									</div>
								</div>
						    </div>
						    <div class="layui-tab-item">
						    	<div class="layui-form-item">
									<label class="layui-form-label">配件商品</label>
									<div class="layui-input-block">
									<?php foreach ($goods as $k=>$v){?>
										<input type="checkbox" name="data[gid][<?php echo $k;?>]" title="<?php echo $v['tgsname'].'(¥'.$v['tgsmoney'].')'?>" <?php echo in_array($v['id'], explode(',', $item['gid']))?'checked':''?> value="<?php echo $v['id']?>">
									<?php }?>
									</div>
								</div>
						    </div>
						  </div>
						</div>	
						<div class="layui-form-item">
							<div class="layui-input-block">
								<input type="hidden" name="id" value="<?php echo $item['id']?>"> 
								<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url' ")?>
							</div>
						</div>
			</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script src="<?php echo PLUGIN.'ueditor/ueditor.config.js'?>"></script>
<script src="<?php echo PLUGIN.'ueditor/ueditor.all.min.js'?>"></script>
<script src="<?php echo PLUGIN.'ueditor/lang/zh-cn/zh-cn.js'?>"></script>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$('.f_file').f_upload();
$(function(){
	setTimeout(function(){	 		
		 UE.getEditor('emditor1',{zIndex:998,scaleEnabled:true});
		 UE.getEditor('emditor2',{zIndex:998,scaleEnabled:true});
		 UE.getEditor('emditor3',{zIndex:998,scaleEnabled:true});
		},1000);
});
</script>
<?php echo template('admin/footer');?>