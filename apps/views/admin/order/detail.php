<?php echo template('admin/header');template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<div class="row">
			<div class="sysNotice col">
				<blockquote class="layui-elem-quote title">订单详情</blockquote>
						<table class="layui-table">
						<colgroup><col width="150"><col><col width="150"><col></colgroup>
						<tbody>
							<tr>
								<td>订单编号</td><td ><?php echo $item['order_no'];?></td>
								<td>订单状态</td>
								<td >
									<?php 
										if($item['state']==1){echo "<span class='btn btn-xs btn-info'>待支付</span> ";
										}elseif($item['state']==2){echo "<span class='btn btn-xs btn-success'>待发货</span> ";
										}elseif($item['state']==3){echo "<span class='btn btn-xs btn-warning'>待收货</span> ";
										}elseif($item['state']==6){echo "<span class='btn btn-xs btn-danger'>订单关闭</span> ";
										}elseif($item['state']==5){echo "<span class='btn btn-xs btn-primary'>退货申请</span>";}
									?>
								</td>
							</tr>
							<tr>
								<td>商品名字</td><td ><?php echo ($item['title']);?></td>
								<td>商品数量</td><td ><?php echo ($item['num']);?></td>
							</tr>
							<tr>
								<td>下单时间</td><td ><?php echo format_time($item['addtime']);?></td>
								<td>支付时间</td><td ><?php echo format_time($item['pay_time']);?></td>
							</tr>
							<tr>
								<td>商品单价</td><td><?php echo $item['price'];?></td>
			              		<td>商品总价</td><td ><?php echo $item['prices'];?></td>
							</tr>
							<tr>
								<td>微信单号</td><td colspan="3"><?php echo $item['transaction_id'];?></td>
							</tr>
							<tr>
								<td>快递公司</td><td><?php echo $item['exp_company'];?></td>
			              		<td>快递单号</td><td><?php echo $item['exp_no'];?></td>
							</tr>
							<tr>
								<td>买家留言</td><td colspan="3"><?php echo $item['message'];?></td>
							</tr>
							<tr>
								<td>收货人姓名</td><td><?php echo $item['buy_name'];?></td>
			              		<td>收货人电话</td><td><?php echo $item['buy_mobile'];?></td>
							</tr>
							<tr>
								<td>收货人地址</td><td colspan="3"><?php echo $item['buy_address'];?></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>
		
		<?php if($item['state']==2){?>
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">发货</div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">快递公司</label>
				<div class="layui-input-block">
					<select name="data[exp_company]">
					<?php foreach ($exp as $v){?>
						<option value="<?php echo $v;?>" ><?php echo $v;?></option>
					<?php }?>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">快递单号</label>
				<div class="layui-input-block">
					<input type="text" name="data[exp_no]" class="layui-input" cname="快递单号" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="hidden" name="id" value="<?php echo $item['id']?>">
					<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
		<?php }elseif($item['state']==5){?>
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">确认退货（系统会自动退款到用户零钱中）</div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">退款金额</label>
				<div class="layui-input-block">
					<input type="text" name="data[total]" class="layui-input" value="<?php echo $item['prices']?>" cname="金额" lay-verify="required" >
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="hidden" name="data[id]" value="<?php echo $item['id']?>">
					<input type="hidden" name="data[wx_oid]" value="<?php echo $item['transaction_id']?>">
					<?php echo admin_btn('/adminct/order/back','save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
		<?php }?>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>