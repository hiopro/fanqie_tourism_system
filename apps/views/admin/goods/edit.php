<?php $data['definedcss'] = array(PLUGIN.'umeditor/themes/default/css/umeditor.min'); echo template('admin/header',$data);template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-tab layui-tab-brief" >
				<ul class="layui-tab-title">
				    <li class="layui-this">基础设置</li>
				    <li>商品详情</li>
				    <li>规格设置</li>
				</ul>
				<div class="layui-tab-content">
			    	<div class="layui-tab-item layui-show">
						<div class="layui-form-item">
							<label class="layui-form-label">分类</label>
								<div class="layui-input-block">
									<select name="data[catid]" >
			                        <?php foreach($parent as $v){?>
			                        	<option value="<?php echo $v['id'];?>" <?php if($v['id']==$item['catid']) echo "selected";?>>  <?php echo $v['names'];?></option>
			                        <?php }?>
			                       	</select>
								</div>
						</div>
						<div class="layui-form-item">
							<label class="layui-form-label">名称</label>
							<div class="layui-input-block">
								<input type="text" name="data[names]" class="layui-input" value="<?php echo $item['names'];?>" lay-verify="required">
							</div>
						</div>
						
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">市场价</label>
								<div class="layui-input-inline">
									<div class="input-group">
										<input type="text" name="data[baz_price]" class="layui-input" value="<?php echo $item['baz_price'];?>"   lay-verify="required|number">
									</div>
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">售价</label>
								<div class="layui-input-inline">
									<div class="input-group">
										<input type="text" name="data[price]" class="layui-input" id="price" value="<?php echo $item['price'];?>"  lay-verify="required|number">
									</div>
								</div>
							</div>
						</div>
						
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">库存</label>
								<div class="layui-input-inline">
									<div class="input-group">
										<input type="text" name="data[stock]" class="layui-input" value="<?php echo $item['stock'];?>" id="stock" lay-verify="number"> 
									</div>
								</div>
							</div>
						</div>
						
						<div class="layui-form-item">
							<label class="layui-form-label">商品主图</label>
							<div class="layui-input-block">
								<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file','data-more="3"');?><span class="thumb-say">图片最大宽度640px</span>
								<div class="layui-upload layui-hide">
								  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
								  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
							  	</div>
							  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify="thumb" value="<?php echo $item['thumb'];?>"/>
							</div>
						</div>
									
						<div class="layui-form-item">
							<label class="layui-form-label">轮播图片</label>
							<div class="layui-input-block">
								<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px （如果不上传默认显示主图）</span>
								<div class="layui-upload layui-hide">
								  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
								  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
							  	</div>
							  	<input name="data[thumb_arr]" class="thumb" type="hidden" value="<?php echo $item['thumb_arr'];?>" />
							</div>
						</div>
									
						<div class="layui-form-item">
							<label class="layui-form-label">分享描述</label>
							<div class="layui-input-block">
								<input type="text" name="data[share_describe]" class="layui-input" value="<?php echo $item['share_describe'];?>">
							</div>
						</div>
					</div>
					
					<div class="layui-tab-item">				
						<div class="layui-form-item">
							<label class="layui-form-label">商品详情</label>
							<div class="layui-input-block">
								<script type="text/plain" id="emditor" name="data[content]"><?php echo $item['content'];?></script>
							</div>
						</div>
					</div>
					<div class="layui-tab-item">
			    		<?php echo template('admin/sku');?>
			    	</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<input type="hidden" name="id" value="<?php echo $item['id']?>">
						<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url' ")?>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script src="<?php echo PLUGIN.'umeditor/third-party/template.min.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/umeditor.config.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/umeditor.min.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/lang/zh-cn/zh-cn.js'?>"></script>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
	var um = UM.getEditor('emditor');
	um.setWidth("100%");
	$(".edui-body-container").css("width", "98%");
});
</script>
<?php echo template('admin/footer');?>