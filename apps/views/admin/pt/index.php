<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入标题" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
				<div class="layui-inline">
					<?php echo admin_btn(site_url("$dr_url/dels"),'dels','layui-btn-danger f_dels');?>
				</div>
				<div class="layui-inline f-right">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/order/pid-{{d.id}}'),'','layui-btn-xs','','订单');?>
<?php echo admin_btn(site_url($dr_url.'/edit/id-{{d.id}}'),'edit','layui-btn-xs');?>
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="sth">
<input type="checkbox" lay-text='启用|关闭' lay-skin="switch" lay-filter='open' {{# if(d.state==1){ }} checked {{#  } }}   data-url="<?php echo site_url($dr_url.'/lock/id-{{d.id}}')?>" >
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'sort', title: '排序', width: 80,edit:'text'},
	       {field: 'title', title: '标题'},
	       {field: 'pt_stime', title: '开始时间',toolbar:'<div>{{d.pt_stime}}</div>'},
	       {field: 'pt_etime', title: '结束时间',toolbar:'<div>{{d.pt_etime}}</div>'},
	       {field: 'money', title: '费用', width: 80},
	       {field: 'num', title: '报名人数'},
	       {field: 'integral_plus', title:'积分', width: 80},
	       {field: 'state', title: '状态',toolbar: '#sth', width: 90},
	       {field: 'right', title: '操作',toolbar:'#operation', width: 120}
	       ]],
	limit: 15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
	}
});
layui.table.on('edit(common)', function(obj){
    var value = obj.value ,data = obj.data;
    $.post('<?php echo site_url("$dr_url/sort")?>',{id:data.id,sort:value},function(d){layer.msg(d.message)},'json');
  });
</script>
<?php echo template('admin/footer');?>