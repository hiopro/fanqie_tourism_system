<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入姓名或者帐号" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
				<div class="layui-inline">
					<?php echo admin_btn(site_url("$dr_url/dels"),'dels','layui-btn-danger f_dels');?>
				</div>
				<div class="layui-inline">
							<?php echo admin_btn(site_url("$dr_url/export"), 'exp','layui-btn-normal');?>
				</div>
				<div class="layui-inline f-right">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<table  id="common" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="sth">
<input type="checkbox" lay-text='正常|锁定' lay-skin="switch" lay-filter='open' {{# if(d.state==1){ }} checked {{#  } }}   data-url="<?php echo site_url($dr_url.'/lock/id-{{d.id}}')?>" >
</script>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/edit/id-{{d.id}}'),'edit','layui-btn-xs');?>
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#common',
	id:'common',
	height: 'full-250',
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {type:'numbers',title: 'No.'},
	       {field: 'name', title: '姓名'},
	       {field: 'username', title: '帐号'},
	       {field: 'pwd', title: '密码',event: 'edit_pwd', style:'cursor: pointer;',toolbar:'<div>点击修改</div>'},
	       {field: 'summary', title: '说明'},
	       {field: 'add_time', title: '注册时间',toolbar:'<div>{{Time(d.add_time, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'last_login_time', title: '登录时间',toolbar:'<div>{{Time(d.last_login_time, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'state', title: '锁定',toolbar: '#sth',width: 90},
	       {field: 'right', title: '操作',fixed:'right',toolbar: '#operation'}
	       ]],
	limit: 20,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});
layui.table.on('tool(common)',function(obj){
	var data = obj.data;
	if(obj.event === 'edit_pwd'){// 更新
		var url = '<?php echo site_url($dr_url.'/edit_pwd')?>';
		layer.prompt({
		    formType: 0,
		    title: '请输入六位数以上密码',
		    value: data.sign,
		    maxlength: 18
		}, function(value, index){
			if(value.length < 6){
				layer.msg('小于六位数');return;
			}
			layer.close(index);
			var l = loads();
			$.post(url,{pwd:value,id:data.id},function(data) {
                layer.msg(data.message,{icon: data.state});
                layer.close(l);
            },'json');
		});
	}
});
</script>
<?php echo template('admin/footer');?>
