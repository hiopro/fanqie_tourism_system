<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">角色组</label>
					<div class="layui-input-block">
						<select name="data[role_id]">
						<?php foreach ($role as $k=>$v){?>
							<option value="<?php echo $v['id']?>" ><?php echo $v['name'];?></option>
						<?php }?>
						</select>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">姓名</label>
					<div class="layui-input-block">
						<input type="text" name="data[name]" class="layui-input"  lay-verify='required'>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">帐号</label>
					<div class="layui-input-block">
						<input type="text" name="data[username]" class="layui-input" lay-verify='required' >
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">说明</label>
					<div class="layui-input-block">
						<textarea name="data[summary]" class="layui-textarea"></textarea>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
					</div>
				</div>
		</form>
	</div>
</div>
					

					
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>