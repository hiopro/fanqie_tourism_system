<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入订单号" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
				<?php if(isset($pid)){?>
				<div class="layui-inline">
					<?php echo admin_btn(site_url("$dr_url/export/pid-$pid"), 'exp','layui-btn-normal','','导出当前');?>
				</div>
				<?php }else{?>
				<div class="layui-inline">
					<?php echo admin_btn(site_url("$dr_url/export"), 'exp','layui-btn-normal');?>
				</div>
				<?php }?>
		</blockquote>
		<table  id="common" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="operation">
	<?php echo admin_btn(site_url($dr_url.'/detail/id-{{d.id}}'),'','layui-btn-xs layui-btn-normal','','详');?>
	<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="zt">
{{# if(d.state==1){ }}
<span class='layui-btn layui-btn-xs layui-bg-red'>待支付</span>
{{# }else if(d.state==2){ }}
<span class='layui-btn layui-btn-xs layui-bg-orange'>已报名</span>
{{# }else if(d.state==3){ }}
<span class='layui-btn layui-btn-xs layui-bg-green'>已结束</span>
{{# }else if(d.state==4){ }}
<span class='layui-btn layui-btn-xs layui-bg-cyan'>已退出</span>
{{# }else if(d.state==5){ }}
<span class='layui-btn layui-btn-xs layui-bg-blue'>申请退出</span>
{{# } }}
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#common',
	id:'common',
	height: 'full-250',
	url:'<?php echo isset($pid)?$dr_url."/lists/pid-$pid":$dr_url.'/lists'?>',
	cols: [[
	       {checkbox: true},
	       {type:'numbers',title: 'No.'},
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'oid', title: '订单号'},
	       {field: 'title', title: '订单名'},
	       {field: 'stime', title: '开始时间'},
	       {field: 'etime', title: '结束时间'},
	       {field: 'total', title: '价格'},
	       {field: 'state', title: '状态',toolbar:"#zt", width: 120},
	       {field: 'addtime', title: '下单时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'right', title: '操作',toolbar: '#operation'}
	       ]],
	limit: 20,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});
</script>