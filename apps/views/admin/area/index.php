<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline ">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<form class="layui-form" method="post">
			<table class="layui-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>地区名称</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
                        <?php if (empty($items)){ ?>
						<tr>
							<td class="empty-table-td"><?php echo $emptyRecord;?></td>
						</tr>
                        <?php }else{ foreach ($items as $value){ ?>
                        <tr>
							<td><?php echo $value['id'];?></td>
							<td><?php echo $value['aname'];?></td>
							<td>
									<?php echo admin_btn(site_url($dr_url.'/edit/id-'.$value['id']),'edit','layui-btn-xs');?>
									<?php echo admin_btn(site_url($dr_url.'/del/id-'.$value['id']),'del','layui-btn-xs f_del');?>
							 </td>
						</tr>
                        <?php }}?>
					</tbody>
			</table>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>