<?php  echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">文章分组</label>
					<div class="layui-input-block">
						<select name="data[catid]" >
			            <?php foreach ($cat as $v){?>
			            	<option value="<?php echo $v['id'];?>"> <?php echo str_repeat('├',$v['level']).' '.$v['catname'];?></option>
			            <?php }?>
			        </select>
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">文章标题</label>
					<div class="layui-input-block">
						<input type="text" name="data[title]" class="layui-input" placeholder="文章标题" lay-verify="required">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">文章图片</label>
					<div class="layui-input-block">
						<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say"></span>
						<div class="layui-upload layui-hide">
						  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
						  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
					  	</div>
					  	<input name="data[thumb]" class="thumb" type="hidden" />
					</div>
				</div>
				
				<div class="layui-form-item">
					<label class="layui-form-label">文章内容</label>
					<div class="layui-input-block">
						<script type="text/plain" id="editor" style="width: 100%; height: 500px;" name="data[content]"></script>
					</div>
				</div>
				
				<div class="layui-form-item">
					<label class="layui-form-label">显示</label>
					<div class="layui-input-block">
						<input type="checkbox" name="data[state]" lay-skin="switch" lay-text="是|否" checked>
					</div>
				</div>
				
				<div class="layui-form-item">
					<label class="layui-form-label">访问量</label>
					<div class="layui-input-block">
						<input type="text" name="data[hits]" class="layui-input" placeholder="访问量" value="0" >
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
					</div>
				</div>
	</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script src="<?php echo PLUGIN.'ueditor/ueditor.config.js'?>"></script>
<script src="<?php echo PLUGIN.'ueditor/ueditor.all.min.js'?>"></script>
<script src="<?php echo PLUGIN.'ueditor/lang/zh-cn/zh-cn.js'?>"></script>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
	setTimeout(function(){	 		
		  var ue = UE.getEditor('editor',{zIndex:998,scaleEnabled:true}) ;
		},1000);
});
</script>
<?php echo template('admin/footer');?>