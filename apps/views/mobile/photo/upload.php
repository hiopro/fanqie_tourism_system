<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<style>
img{width: 100%}
</style>
<div id="app">
	<van-nav-bar title="发布相册"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
	<van-row  gutter="10">
		<van-col class="mb5 " span="6" v-for="(v,index) in list">
			<div class="bg_ff img-boder p5 m5 photo">
				<lable class="lable-img" style="height:70px;"><img :src="window.URL.createObjectURL(v)" ></lable>
				<div class="close" @click="onClose(index)"><van-icon name="clear" ></van-icon></div>
			</div>
		</van-col>
	</van-row>
	
	<van-row class="mt15 mb60 text-center unews-u-add ml10 mr10" >
		<van-col  span="24" >
			<div class="bg_ff pt5 pb5" >
				<van-uploader :after-read="addPhoto"  accept="image/*" @oversize="onSize('8M')" max-size="8388608" multiple >
					<p><i class="iconfont f30 icon-xiangji"></i></p>
					<p>添加照片</p>
				</van-uploader>
			</div>
		</van-col>
	</van-row>

	<div class="mt60">&nbsp;</div>
	<van-row class="van-contact-list-bottom ">
	 	  <van-col span="24">
		    <van-button bottom-action :loading="sub_load" type="danger" @click="submit">提交发布</van-button>
		  </van-col>
	</van-row>
	
	<van-popup v-model="show_load" style="background-color:rgba(255, 255, 255, 0);" :close-on-click-overlay="false" :z-Index = "10000">
		<van-circle  v-model="progresss"  :rate="progress" :speed="100" color="#13ce66" :text="load_text" fill="#fff"  size="120px"  layer-color="#eee"></van-circle>
	</van-popup>
	
</div>

<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/f_yasuo.js'?>" ></script>
<script>
var id = <?php echo $id;?>;
var config = {
	onUploadProgress: progressEvent => {
		   var complete = (progressEvent.loaded / progressEvent.total * 100 | 0);
		   r.progress = complete;
		   r.load_text = r.progress+'%';
	}
}
var r = new Vue({
	el: '#app',
	data: {
		list:[],progress:0,progresss:0,show_load:false,load_text:'',sub_load:false
	},
  	methods: {
  	  	submit(){
  	  	  	if(this.list.length <= 10){
  	  	  	  	this.show_load = true;
  	  	  	  	var fd = new FormData();
  	  	  	  	fd.append('id',id);
  	  	  	  	console.log(this.list);
	  	  	  	for(var i=0;i < this.list.length;i++){
	  	  	  	  	fd.append('file[]',this.list[i],this.list[i].name);
	  	  	  	}
	  	  	  	axios.post('/mobile/photo/uploads',fd,config).then((response)=> {
	 	  	  	  	var data = response.data;
	 	  	      	if(data.state==1){
// 	 	 	  	      	location.href = "/mobile/photo/lists/id-"+id;
	 	  	  	  	}
	 	 	  	    this.$toast(data.message);	
	 		  	    this.progress = 0;
	 		  	    this.show_load = false;
	 	  	    });
  	  	  	}else{
  	  	  	  	this.$toast("一次最多上传10张图片");
  	  	  	}
  	  	},
  		onClose(index){
  			this.list.splice(index,1)
  	  	},
        onSize(size){
        	this.$toast("文件过大，不能超过"+size);
        },
        addPhoto(file){
            if(file.length > 4){
                this.$toast("一次最多添加4张图片");
            }else{
                var arr = [],html = '';
                if(!file.length){
                    arr.push(file)
                }else{
                    arr = file;
                }
                arr.map(uploads);
            }
        }
  	}
});

function uploads(file) {
	var img = new Image(); 
    img.src = getObjectURL(file.file);     
    img.onload = ()=>{
    	compressSrc = compress(img,0.5,100,file.file);
      	r.list.push(compressSrc);
    }
}
function getObjectURL(file) {
    var url = null ;
    if (window.createObjectURL!=undefined) { // basic
        url = window.createObjectURL(file) ;
    } else if (window.URL!=undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file) ;
    } else if (window.webkitURL!=undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file) ;
    }
    return url ;
}
</script>
</body>
</html>