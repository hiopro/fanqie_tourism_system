<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="积分明细"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
	<template v-if="list">
	<van-cell-group>
	  	<van-cell v-for="(v,index) in list" :title="v.src" :value="v.ands+v.num" :label="v.addtime" ></van-cell>
	</van-cell-group>
	</template>
	<template v-else><p class="cr_hs2 text-center mt60"><i class="iconfont icon-meiyoujieguo f80 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
	
</div>
<div class="mb60">&nbsp;</div>

<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		list:<?php echo $items;?>
	},
  	methods: {
  		
  	},
  	mounted:function (){
  		
	}
});
</script>
</body>
</html>