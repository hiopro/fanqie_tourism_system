<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="积分说明"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	<van-row class="p10 bg_ff">
	  <van-col span="24">
	  	<?php echo $item['content'];?>
	  </van-col>
	</van-row>
	<van-button type="primary" bottom-action class="van-contact-list-bottom" @click="tz" text="进入我的积分"></van-button>  
</div>
<div class="mb60">&nbsp;</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	methods:{		
  	  	tz(){
  	  	  	location.href = "/mobile/user/integral_list.html";
  	  	}  
	}
});
</script>
</body>
</html>