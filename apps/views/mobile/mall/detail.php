<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-swipe :autoplay="3000">
      <van-swipe-item v-for="thumb in lbthumb" :key="thumb" class="text-center">
        <img v-lazy="thumb" >
      </van-swipe-item>
    </van-swipe>
    <van-cell-group>
      <van-cell>
        <div class="goods-title" v-text="goods.names"></div>
        <div class="goods-price" v-text="goods.price"></div>
      </van-cell>
      <van-cell class="cr_hs2">
        <van-col span="10">运费：0</van-col>
        <van-col span="14">剩余：{{ goods.stock }}</van-col>
      </van-cell>
    </van-cell-group>
    <van-cell-group class="mt10">
      <van-cell  icon="shop" is-link >
        <template slot="title">
          <span class="van-cell-text">旅游商城</span>
          <van-tag type="danger">官方直营</van-tag>
        </template>
      </van-cell>
    </van-cell-group>
    <van-cell-group class="mt10">
      <van-cell title="查看商品详情" is-link />
    </van-cell-group>
  	<van-sku v-model="showBase" :sku="sku"  :goods="goodss"  :goods-id="goodsId"  :hide-stock="sku.hide_stock"  @buy-clicked="onBuyClicked"  @add-cart="onAddCartClicked" disable-stepper-input="false"></van-sku>
    <van-goods-action>
      <van-goods-action-mini-btn icon="home" text="首页" url="/mobile/mall/index.html"></van-goods-action-mini-btn>
      <van-goods-action-mini-btn icon="cart" text="购物车" url="/mobile/car/index.html"></van-goods-action-mini-btn>
      <van-goods-action-big-btn @click="showBase = true" text="加入购物车"></van-goods-action-big-btn>
      <van-goods-action-big-btn primary @click="showBase = true" text="立即购买"></van-goods-action-big-btn>
    </van-goods-action>
</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		sku_show:false,showBase:false,
		goods:<?php echo json_encode($item);?>,
		lbthumb:<?php echo json_encode(explode(',', $item['thumb_arr']))?>,
		sku: <?php echo json_encode($sku);?>,
		goodsId:<?php echo $item['id'];?>,
		goodss: <?php echo json_encode(array('title'=>$item['names'],'picture'=>explode(',', $item['thumb_arr'])[0]));?>
	},
  	methods: {
  		edituser(){
  	  		location.href = "/mobile/user/edit.html";
  	  	},
		edit(){
			this.$toast.fail("完善中...");
		},
		onBuyClicked(res){
			var l = this.$toast.loading({duration: 0,mask: true,message: '提交中...'});
  			axios.post('/mobile/mall/save_goods', Qs.stringify(res),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	    	location.href = "/mobile/mall/order.html";
  	  	  	  	}
  	  	  	    this.$toast(data.message);
  	  	    });
		},
		onAddCartClicked(res){
			var l = this.$toast.loading({duration: 0,mask: true,message: '添加中...'});
  			axios.post('/mobile/car/add_car', Qs.stringify(res),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	  	    this.$toast(data.message);
  	  	    });
		}
  	},
  	mounted:function (){
  		
	}
});
</script>
</body>
</html>