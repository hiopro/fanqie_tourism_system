<script>
function jsApiCall(json){
	WeixinJSBridge.invoke(
		'getBrandWCPayRequest',json,
		function(res){
			switch (res.err_msg){
	            case 'get_brand_wcpay_request:cancel':
	            	vant.Toast("取消支付");
	             	break;
	            case 'get_brand_wcpay_request:fail':
	            	vant.Toast("支付异常,截图联系客服"+res.err_code+res.err_desc+res.err_msg);
	                break;
	            case 'get_brand_wcpay_request:ok':
	            	vant.Toast("支付成功");
	            	window.location.href = "/mobile/user/index";
	                break;
			}
		}
	);
}
function callpay(data){
	if(data.state==1){
		if (typeof WeixinJSBridge == "undefined"){
		    if( document.addEventListener ){
		        document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
		    }else if (document.attachEvent){
		        document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
		        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
		    }
		}else{
		    jsApiCall(data.data);
		}
	}
}
</script>