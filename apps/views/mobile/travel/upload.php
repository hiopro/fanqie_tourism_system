<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $data['definedcss'] = array(CSS_PATH.'mobile/sweetalert2.min',CSS_PATH.'mobile/index'); echo template('mobile/header',$data);?>
<div class="top flex-csb bg-ff plr12 ptb8 mb15">
		<a href="javascript:history.back()">
			<svg class="icon c-diy" aria-hidden="true" ><use xlink:href="#icon-zuojiantou"></use></svg>
		</a>
		<span class="f15 fw-bold c-diy">发布游记</span>
		<a href="/">
			<svg class="icon c-diy" aria-hidden="true" font-size="0.25rem"><use xlink:href="#icon-shouye-copy"></use></svg>
		</a>
</div>
<form id='add'>
<div class="upload-pane bg-ff clearfix mt15">
	<textarea name="content" id="travel-content" rows="10" class="w100 p8"  placeholder="说出你的旅游感想吧" required></textarea>
	<label class="left w3 p8 upload-list p-r">
		<input type="file" class="upload" accept="image/*" multiple  onchange="previewImg(this)" hidden/>
		<input type="hidden" name="thumb[]" class="thumb_val">
		<img src="<?php echo IMG_PATH.'mobile/upload.png'?>">
		<button class="delete-btn p-a br-circle flex-center" type="button" style="display:none">
			<svg class="icon" aria-hidden="true" color="#fff" font-size="0.13rem">
				<use xlink:href="#icon-cross"></use>
			</svg>
		</button>
	</label>
</div>
</form>
<a class="bottom-fixed bg-primary f16 cf ta-center ptb12 ajaxproxy" href="<?php echo site_url('mobile/travel/save')?>" formId="add" location="<?php echo site_url('mobile/travel/index')?>" >确认发表</a>
<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/travel_img.js'?>" ></script>
<script>
	// 相片删除
	$('.upload-pane').on('click','.delete-btn',function(){
		var that = this;
		 $.ajax({
 				type:'post',
 				url:$(this).data('url'),
 				data:{thumb:$(this).prev().attr('src')},
 				dataType:'json',
 				success:function(data){
 					if(data.state==1){
 						$(that).parent().remove();
 					}
 					layer_msg(data.message);
 				}
 		})
	})
</script>
</body>
</html>