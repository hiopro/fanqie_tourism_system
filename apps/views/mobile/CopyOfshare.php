<script src="<?php echo JS_PATH.'jweixin-1.2.0.js'?>"></script>
<script type="text/javascript">
wx.config(<?php echo $wxconfig; ?>);
wx.ready(function () {
	wx.onMenuShareTimeline({
	    title: '<?php echo $share_data['title'];?>', // 分享标题
	    link: '<?php echo $share_data['link'];?>', // 分享链接
	    imgUrl: "<?php echo $share_data['img'];?>"
	});
	wx.onMenuShareAppMessage({
	    title: '<?php echo $share_data['title'];?>', // 分享标题
	    desc: '<?php echo $share_data['desc'];?>', // 分享描述
	    link: "<?php echo $share_data['link'];?>", // 分享链接
	    imgUrl: '<?php echo $share_data['img'];?>'
	});
	<?php if(!$U['location']){?>
	vant.Toast.loading({duration: 0,mask: true,message: '定位中...'});
	wx.getLocation({
		type: 'wgs84', // 可传入'gcj02'
		success: function (res) {
			var la = res.latitude; 
			var lg = res.longitude;
			dingw(la,lg);
		}
	});
	<?php }?>
});

</script>
</script>