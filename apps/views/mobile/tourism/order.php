<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">

	<van-nav-bar title="填写报名人信息"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
	<div class="order-contact">
		<van-contact-cards  :type="cardType"  :xz="currentContact"   @click="onInit" ></van-contact-cards>
		<van-popup v-model="showList" position="bottom">
		  	<van-contact-lists  v-model="ckb"  :list="list" @add="onAdd" @edit="onEdit" @close="showList = false" ></van-contact-lists>
		</van-popup>
		<van-popup v-model="showEdit" position="bottom">
  			<van-contact-edits :contact-info="editingContact" :is-edit="isEdit" :is-saving="saving" :is-deleting="deleting" @save="onSave" @delete="onDelete" @close="showEdit = false" ></van-contact-edits>
		</van-popup>
	</div>
	
	<van-cell-group class="mt10">
		<van-cell ><van-tag mark type="danger">费用详情</van-tag></van-cell>
	  	<van-cell title="基础价格"  ><span class="cr-full" v-html="moneys"></span> 元/人</van-cell>
	</van-cell-group>
	
	<template v-if="goods_list">
		<van-cell-group class="mt10 order-goods">
		  <van-cell ><van-tag mark type="danger">商品服务</van-tag></van-cell>
		  <van-cell v-for="(v, index) in goods_list">
		  	  <van-col span="13" >
		  	  	<span v-text="v.tgsname" >{{v.tgsname}}</span> <van-icon name="photo" class="icon-bottom cr_main" @click="onSee(v.tgsthumb)"></van-icon>
		  	  </van-col>
		  	  <van-col span="5" > 
		  	  	<span v-text="'+ '+v.tgsmoney" class="cr-full"></span>
		  	  </van-col>
			  <van-col span="6" class="text-right">
			  	<van-stepper :min="0" :disable-input="true" :default-value="0" @plus="onPlus(v.tgsmoney)" @minus="onMinus(v.tgsmoney)" @change="onGoods(v.id,$event)"></van-stepper>
			  </van-col>
		  </van-cell>
		</van-cell-group>
	</template>
	
	<van-radio-group v-model="payRadio">
		<van-cell-group class="mt10 order-pay">
			<van-cell ><van-tag mark type="danger">支付方式</van-tag></van-cell>
			<van-cell><van-radio name="1" >微信支付</van-radio></van-cell>
		</van-cell-group>
	</van-radio-group>
	
	<van-cell-group class="mt10 mb60 order-goods">
		<van-cell ><van-tag mark type="danger">优惠抵扣</van-tag> <van-icon slot="right-icon" name="question" class="van-cell__right-icon f16" ></van-icon></van-cell>
		<van-cell title="使用积分" is-link @click="onIntergal"><div v-html="my_intergals"></div></van-cell>
	</van-cell-group>
	
	<van-dialog v-model="IntergalShow" @confirm="onConfirm" show-cancel-button="true" title="积分抵扣" >
	  <van-field v-model="exchangeVal" type="tel" @input="onExchangeVal(exchangeVal)" placeholder="请输入积分" ></van-field>
	  <div class="van-dialog__message van-dialog__message--withtitle" >可抵扣 {{exchangeVals}} 元 （总积分 {{my_intergal}}）</div>
	</van-dialog>
	
	<van-submit-bar  :price="total" class="bg-main" button-text="支付" button-type="primary"  :disabled="total?false:true" @submit="onSubmit"></van-submit-bar>
</div>
<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/contact.js'?>" ></script>
<script type="text/javascript" src="<?php echo JS_PATH.'jquery.min.js'?>" ></script>
<script>
var uid = <?php echo $U['id']?>;
var id = <?php echo $id?>;
var cid = <?php echo $cid?>;
new Vue({
	el: '#app',
	data: {
	     editingContact: {},showList: false,showEdit: false,isEdit: false,ckb:[],list: [],saving:false,deleting:false,
	     money:<?php echo $item['money'];?>,moneys:'<?php echo $item['money'];?>',goods:new Array(),
	     goods_list:'',payRadio: '1',my_intergal:<?php echo $integral?$integral:0;?>,my_intergals:'',IntergalShow:false,exchange:0,exchangeVal:'',exchangeVals:0,total:0,goodsPrice:0,exchangeValok:0
	},
	computed: {
	    cardType() {
	      return this.ckb.length !=0  ? 'edit' : 'add';
	    },
	    currentContact() {
		  if(this.ckb.length > 0){
			  this.total = this.goodsPrice + (this.ckb.length*this.money * 100) - (this.exchangeValok * 100);
		  }else{
			  this.total = 0;
		  }
	      return this.ckb;
	    }
	},
  	methods: {
  		onSubmit(){
  	  		var data = [];
  	  		data['id'] = id;data['cid'] = cid;
  	  	  	data['info'] = JSON.stringify(this.ckb);
  	  	  	if(this.goods.length>1){
  	  	  	  	var s = this.goods.filter(t => t.id != id);
  	  	  	  	data['goods'] = JSON.stringify(s);
  	  	  	}
  	  	  	data['integral'] = this.exchangeVal;
  	  	  	var l = this.$toast.loading({duration: 0,mask: true,message: '支付中...'});
  			axios.post('/mobile/tourism/pay', Qs.stringify(data),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	      	callpay(data);
  	  	  	  	}else{
					this.$toast(data.message);
  	  	  	  	}
  	  	    });
  	  	},
  	  	onGoods(id,event){
  	  	  	if(event==0){
  	  	  	  	this.goods.splice($.inArray(id, this.goods), 1);
  	  	  	}else{
  	  	  	  	this.goods[id] = {id:id,num:event};
  	  	  	}
  	  	},
  		onPlus(price){
  	  	  	this.goodsPrice += price*100;
  	  	},
    	onMinus(price){
  	  	  	this.goodsPrice -= price*100;
  	  	},
  		onExchangeVal(v){
  	  		if(v.length > 6){
  	  	  		this.exchangeVal = v.slice(0,6);
  	  	  	}else{
  	  	  	  	var o = /^[1-9]\d*$/;
	  	  		var r = o.test(v);
	  	  		if(r){
		  	  		this.exchangeVals =  v * 1000000 * this.exchange/1000000;
		  	  	}else{
			  	  	this.exchangeVal = '';
			  	}
  	  	  	}
  	  	},
  		onConfirm(){
  	  		if(!this.exchangeVal)return ;
  	  		if(this.exchangeVal > this.my_intergal){
  	  	  		this.$toast("已超过您的可用积分");
  	  	  	  	this.IntergalShow = true;
  	  	  	}else{
  	  	  	  	if(this.total <= (this.exchangeVals*100)){
  	  	  	  	  	this.$toast("积分使用过大");
  	  	  	  		this.my_intergals = "";
  	  	  	  	    this.IntergalShow = true;
  	  	  	  	}else{
  	  	  	  	  	this.my_intergals  = '抵扣 <span class="cr-full"> - '+this.exchangeVals+'</span> 元';
  	  	  	  		this.exchangeValok = this.exchangeVals;
  	  	  	  	}
  	  	  	}
  	  	},
  		onIntergal(){
  	  		this.exchange = <?php echo $item['exchange']?>;
  	  		if(this.exchange){
  	  	  		if(this.total){
  	  	  	  		this.exchangeValok = 0;
  	  	  	  		this.IntergalShow = true;
  	  	  	  	}else{
  	  	  	  	  this.$toast('请先选择联系人');
  	  	  	  	}
  	  	  	}else{
  	  	  	  	this.$toast('此活动未开启积分兑换');
  	  	  	}
  	  	},
  		onSee(thumb){
  			vant.ImagePreview([thumb]);
  	    },
  	  	onInit(){
  	  	  	this.showList = true;
  	  	  	if(this.list.length==0){
  	  	  	  	axios.post('/api/contact/lists', Qs.stringify({uid:<?php echo $U['id']?>}),ajaxconfig).then((response)=> {
  	  	  	  	  	var data = response.data;
  	  	  	      	if(data.state==1){
  	    	  	   		this.list = data.data;
  	  	  	  	  	}else{
  						this.$toast(data.message);
  	  	  	  	  	}
  	  	  	    });
  	  	  	}
  	  	},
  	    onAdd() {
  	      	this.editingContact = { id: this.list.length };    	   
  	      	this.isEdit = false;
  	      	this.showEdit = true;
  	    },
  	    onEdit(item) {
  	    	this.isEdit = true;      
  	      	this.showEdit = true;
  	      	this.editingContact = item;
  	    },
  	    onSave(info) {
  	      	this.saving = true;
  	      	if (this.isEdit) {
  	        	axios.post('/api/contact/edit', Qs.stringify({id:info.id,'d[username]':info.username,'d[mobile]':info.mobile,'d[code]':info.code}),ajaxconfig).then((response)=> {
  		  	  	  	var data = response.data;
  		  	      	if(data.state==1){
  	  		  	      	this.list = this.list.map(item => item.id === info.id ? info : item);
  		  	  	  	}else{
  						this.$toast(data.message);
  		  	  	  	}
  	  		  	    this.saving = false;
  	  	  		  	this.showEdit = false;
  		  	    });
  	        	if(this.ckb.length!=0){
  	    	  	   	this.ckb = this.ckb.filter(item => item.id === info.id ? info : item);
  	    		}
  	      	} else {
  	  	      	axios.post('/api/contact/add', Qs.stringify({'d[uid]':uid,'d[username]':info.username,'d[mobile]':info.mobile,'d[code]':info.code}),ajaxconfig).then((response)=> {
		  	  	  	var data = response.data;
		  	      	if(data.state==1){
			  	      	this.list.push(info);
		  	  	  	}else{
						this.$toast(data.message);
		  	  	  	}
	  		  	    this.saving = false;
	  	  		  	this.showEdit = false;
		  	    });
  	      	}
  	    },
  	    onDelete(info) {
  	    	this.deleting = true;
  	    	axios.post('/api/contact/del', Qs.stringify({id:info.id}),ajaxconfig).then((response)=> {
	  	  	  	var data = response.data;
	  	      	if(data.state==1){
		  	      	this.list = this.list.filter(item => item.id !== info.id);
		  	      	if(this.ckb.length!=0){
		    	    	this.ckb = this.ckb.filter(item => item.id !== info.id);
		  	  	  	}
	  	  	  	}else{
					this.$toast(data.message);
	  	  	  	}
  		  	    this.deleting = false;
  	  		  	this.showEdit = false;
	  	    });
  	      	
  	    }
  	},
  	created:function (){
  		<?php if($item['gid']){ ?>
  		axios.post('/api/home/tgoods_lists', Qs.stringify({gid:'<?php echo $item['gid']?>'}),ajaxconfig).then((response)=> {
  	  	  	var data = response.data;
  	      	if(data.state==1){
	  	   		this.goods_list = data.data;
  	  	  	}
  	    });
  	    <?php }?>
	}
});

</script>
<?php echo template('mobile/pay');?>
</body>
</html>