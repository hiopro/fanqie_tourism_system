<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="提现"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	<van-cell-group>
	  <van-field v-model="tmoney"  clearable label="金额：" @input="onExchangeVal(tmoney)"  placeholder="请输入提现金额" ></van-field>
  	</van-cell-group>
  	<van-panel title="提现说明：" class="mt10">
	  <div class="p10 cr_888">	  
	  	<p>1、提现后会进入审核状态</p>
	  	<p>2、审核成功，提现款会自动打入您的微信零钱</p>
	  	<p>3、最小提现金额为100元</p>
	  	<p>4、如有提现问题请咨询客服</p>
	  </div>
	</van-panel>
  	<van-button type="primary" bottom-action class="van-contact-list-bottom" @click="sub" text="确认提现"></van-button>  
</div>

<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		tmoney:''
	},
  	methods: {
  		sub(){
  	  		if(this.tmoney&&this.tmoney>=100){
  	  	  		var l = this.$toast.loading({duration: 0,mask: true,message: '申请中...'});
  	  			axios.post('/mobile/fx/withdraw_sub', Qs.stringify({money:this.tmoney}),ajaxconfig).then((response)=> {
  	  	  	  	  	var data = response.data;
  	  	  	  	  	l.clear();
  	  	  	      	if(data.state==1){
  	  	  	  	      	this.$toast.success(data.message);
  	  	  	  	    	location.href = '/mobile/fx/index.html';
  	  	  	  	  	}else{
  	    	  	  	  	this.$toast.fail(data.message);
  	  	  	  	  	}
  	  	  	  	    
  	  	  	    });
  	  	  	}else{
  	  	  	  	this.$toast.fail("金额错误");
  	  	  	}
  	  	},
    	onExchangeVal(v){
  	  		if(v.length > 6){
  	  	  		this.tmoney = v.slice(0,6);
  	  	  	}else{
  	  	  	  	var o = /^[1-9]\d*$/;
	  	  		var r = o.test(v);
	  	  		if(r){
		  	  		this.tmoney =  v ;
		  	  	}else{
			  	  	this.tmoney = '';
			  	}
  	  	  	}
  	  	}
  	},
  	mounted:function (){
  		
	}
});
</script>
</body>
</html>