<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-row class="text-center cr_ff fx_header mb10">
		<van-col span="24" ><van-icon name="points" class="icons"></van-icon></van-col>
		<van-col span="24" class="money" v-text="'￥<?php echo $total?>'"></van-col>
	</van-row>
	
	<van-cell-group>
    	<van-cell icon="qr" title="我的二维码" is-link url="/mobile/fx/qrcode.html" ></van-cell>
      	<van-cell icon="contact" title="我的团队" is-link url="/mobile/fx/subordinate.html" ></van-cell>
      	<van-cell icon="gold-coin" title="收益明细" is-link url="/mobile/fx/earnings.html" ></van-cell>
      	<van-cell icon="exchange-record" title="我要提现" is-link url="/mobile/fx/withdraw.html" ></van-cell>
      	<van-cell icon="arrow-left" title="返回上一页" is-link url="/mobile/user/index.html" ></van-cell>
	</van-cell-group>
</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app'
});
</script>
</body>
</html>