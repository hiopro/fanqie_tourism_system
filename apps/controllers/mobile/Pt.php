<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Order;
class Pt extends WechatCommon {
	
	function __construct(){
		parent::__construct();
	}
	
	function index() {
		$data['lid'] = $this->User['location'];
		$items = get_Cache('tourism_tag');
		if(!$items)showmessage('数据异常','error');
		$data['items'] = json_encode($items);
		$this->load->view('mobile/pt/index',$data);
	}
	
	function detail() {
		$data['id'] = $id = Gets('id','num');
		$this->load->model(array('admin/Tourism_model'));
		$item = $data['item'] = $this->Tourism_model->getItem(array('id'=>$id));
		$diff = get_diff();
		$data['item']['diff_say'] = $diff[$item['diff']];//难度解析
		$data['item']['integral_plus_say'] = $data['item']['integral_plus']?'+'.$data['item']['integral_plus'].' 积分':'';
		//分享
		$this->load->vars('share_data',array('title'=>$item['s_title']?$item['s_title']:$item['title'],'img'=>base_url($item['s_thumb']?$item['s_thumb']:$item['thumb']),'desc'=>$item['s_desc']?$item['s_desc']:$item['title'],'link'=>base_url('mobile/pt/detail/id-'.$item['id'])));
		if($item['share_plus']&&$item['share_plus_day']){
			$data['ajax'] = site_url('mobile/share/pt/id-'.$item['id']);
		}
		$this->load->view('mobile/pt/detail',$data);
	}
	
	function time(){
		$id = Gets('id','num');
		$this->load->model(array('admin/Tourism_model','admin/TourismChild_model'));
		$data['item'] = $this->Tourism_model->getItem(array('id'=>$id),'id,day_num');
		$data['items'] = $this->TourismChild_model->getItems(array('tid'=>$id),'stime');
		$this->load->view('mobile/pt/time',$data);
	}
	

	function tourism_apply(){
		if(is_ajax_request()){
			$user = $this->User;
			$say = Posts('say');
			$this->load->model('admin/TourismApply_model');
			$result = $this->TourismApply_model->add(array('uid'=>$user['id'],'nickname'=>$user['nickname'],'content'=>$say,'addtime'=>time()));
			is_AjaxResult($result);
		}
		
	}
}
