<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
use EasyWeChat\Foundation\Application;
class Fx extends WechatCommon {
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin/User_model");
		$user = $this->User_model->getItem(array('id'=>$this->User['id']));
		if($user['gid']!=2)showmessage('您不是分销会员','error');
	}
	
	public function index() {
		$uid = $this->User['id'];
		$this->load->model("admin/Earnings_detail_model");
		$withdraw = $this->Earnings_detail_model->getItems(array('uid'=>$uid,'state'=>0));
		$total = 0;
		foreach ($withdraw as $v){
			if($v['ands']=='+'){
				$total += $v['money'];
			}else{
				$total -= $v['money'];
			}
		}
		$data['total'] = $total;
		$this->load->view('mobile/fx/index',$data);
	}
	
	function qrcode(){
		$uid = $this->User['id'];
		$img = IMG_PATH."mobile/qrcode/$uid.jpg";
		$data['img'] = $this->downImage($img, $uid);
		$this->load->view ('mobile/fx/qrcode',$data);
	}
	
	private function downImage($img,$uid){
		if(!is_file(substr($img,1))){
			$config = array('app_id'=>WX_APPID,'secret'=>WX_APPSecret);
			$app = new Application($config);
			$result = $app->qrcode->forever($uid);
			$url = $app->qrcode->url($result['ticket']);
			$content = file_get_contents($url);
			$filename = FCPATH.$img;
			file_put_contents($filename, $content);
		}
		return $img;
	}
	
	//下级
	function subordinate(){
		if(is_ajax_request()){
			$uid = $this->User['id'];
			$id = Posts('id','num')+1;
			$this->load->model('admin/User_model');
			$result = $this->User_model->getItems("p_$id=$uid");
			if($result){
				AjaxResult(1, '',result_format_time($result,'Y-m-d'));
			}else{
				AjaxResult_error('没有数据');
			}
		}else{
			$this->load->view ('mobile/fx/subordinate');
		}
	}
	
	//提现
	function withdraw(){
		$uid = $this->User['id'];
		$this->load->model("admin/Earnings_detail_model");
		$item = $this->Earnings_detail_model->getItem("state=1 and uid=$uid");
		if($item)showmessage('你有一笔提现正在审核中','waiting');
		$this->load->view('mobile/fx/withdraw');
	}
	
	function withdraw_sub(){
		if(is_ajax_request()){
			//对比提交数据
			$money = Posts('money','checkid');
			if($money<100)AjaxResult_error('最小提现金额为100元');
			$uid = $this->User['id'];
			$this->load->model("admin/Earnings_detail_model");
			$withdraw = $this->Earnings_detail_model->getItems(array('uid'=>$uid,'state'=>0));
			if(!$withdraw)AjaxResult_error('金额错误');
			$total = 0;
			foreach ($withdraw as $v){
				if($v['ands']=='+'){
					$total += $v['money'];
				}else{
					$total -= $v['money'];
				}
			}
			if($total >= $money){
				$data = array('uid'=>$uid,'cid'=>0,'money'=>$money,'src'=>'提现','ands'=>'-','state'=>1,'addtime'=>time());
				$this->Earnings_detail_model->add($data);
				AjaxResult_ok("提现成功");
			}else{
				AjaxResult_error("金额不足");
			}
		}
	}
	
	//明细
	function earnings(){
		$uid = $this->User['id'];
		$this->load->model(array('admin/Earnings_detail_model'));
		$result = $this->Earnings_detail_model->getItems("uid=$uid","",'id desc');
		if($result){
			foreach ($result as $v){
				$state = '';
				if($v['state']==1){
					$state = "审核中";
				}elseif($v['state']==2){
					$state = "提现失败";
				}
				$v['addtime'] = format_time($v['addtime'],'Y-m-d H:i').' '.$state;
				$results[] = $v;
			}
		}
		$data['items'] = $result?json_encode($results):0;
		$this->load->view ('mobile/fx/earnings',$data);
	}
}
