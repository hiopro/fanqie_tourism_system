<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Share extends WechatCommon {
	
	function index() {
		$this->load->view('mobile/tourism/detail');
	}
	
	function tourism(){
		$uid = $this->User['id'];
		$id = Gets('id','num');
		$this->load->model(array('admin/Tourism_model','admin/User_model','admin/Integral_model'));
		$item = $this->Tourism_model->getItem(array('id'=>$id),'share_plus,share_plus_day');
		$user = $this->User_model->getItem(array('id'=>$uid),'share,share_time,nickname');
		if($item['share_plus']&&$item['share_plus_day']){
			$c = time() - strtotime(date('Y-m-d',$user['share_time']).'23:59:59');
			
			if($c&&$user['share_time']){
				$this->User_model->updates_se(array('share_time'=>time(),'share'=>0),array('id'=>$uid));
			}else{
				
				if($item['share_plus_day'] > $user['share']){
					$this->User_model->updates_se(array('integral'=>'+='.$item['share_plus']),array('id'=>$uid));
					$this->Integral_model->add(array('uid'=>$uid,'nickname'=>$user['nickname'],'num'=>$item['share_plus'],'src'=>'分享','ands'=>'+','addtime'=>time()));
				}
			}
		}
	}
	
	function pt(){
		$uid = $this->User['id'];
		$id = Gets('id','num');
		$this->load->model(array('admin/Pt_model','admin/User_model','admin/Integral_model'));
		$item = $this->Pt_model->getItem(array('id'=>$id),'share_plus,share_plus_day');
		$user = $this->User_model->getItem(array('id'=>$uid),'share,share_time,nickname');
		
		if($item['share_plus']&&$item['share_plus_day']){
			$c = time() - strtotime(date('Y-m-d',$user['share_time']).'23:59:59');
			if($c&&$user['share_time']){
				$this->User_model->updates_se(array('share_time'=>time(),'share'=>0),array('id'=>$uid));
			}else{
				if($item['share_plus_day'] > $user['share']){
					$this->User_model->updates_se(array('integral'=>'+='.$item['share_plus']),array('id'=>$uid));
					$this->Integral_model->add(array('uid'=>$uid,'nickname'=>$user['nickname'],'num'=>$item['share_plus'],'src'=>'分享','ands'=>'+','addtime'=>time()));
				}
			}
		}
	}
	
	function common(){
		$uid = $this->User['id'];
		$id = Gets('id','num');
		$this->load->model(array('admin/User_model','admin/Integral_model'));
		$item = admin_config_cache('wechat');
		$user = $this->User_model->getItem(array('id'=>$uid),'share,share_time,nickname');
	
		if($item['wechat_integral']){
			$c = time() - strtotime(date('Y-m-d',$user['share_time']).'23:59:59');
			if($c&&$user['share_time']){
				$this->User_model->updates_se(array('share_time'=>time(),'share'=>0),array('id'=>$uid));
			}else{
				if($user['share'] > 1){
					$this->User_model->updates_se(array('integral'=>'+='.$item['wechat_integral']),array('id'=>$uid));
					$this->Integral_model->add(array('uid'=>$uid,'nickname'=>$user['nickname'],'num'=>$item['wechat_integral'],'src'=>'分享','ands'=>'+','addtime'=>time()));
				}
			}
		}
	}
	
	
	
}
