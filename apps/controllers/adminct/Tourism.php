<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 旅游
 * @author chaituan@126.com
 */
class Tourism extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Tourism_model'=>'do','admin/User_model'=>'do_user'));
	}
	
	public function config() {
		$data = get_Cache('admin_config');
		foreach ( $data as $v ) {
			if ($v ['tkey'] == 'tourism') {
				$datas ['items'][] = $v;
			}
		}
		$this->load->view ('admin/config/views', $datas);
	}
	
	public function index() {
		$data['day_num'] = array(1,2,3,4,5,6,7,8,9,10);
		$this->load->view('admin/tourism/index',$data);
	}
	
	function lists(){
		$srk = Gets('srk');$num = Gets('num');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');$total = Gets('total','num');
		$where = '';
		if($srk)$where['title like'] = "%$srk%";
		if($num){
			if($num>9){
				$where['day_num >='] = $num;
			}else{
				$where['day_num'] = $num;
			}
		}
		$data = $this->do->getItems($where,'','sort,id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($srk&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request()) {
			$data = Posts('data');
			$data['gid'] = isset($data['gid'])?implode(',', $data['gid']):'';
			$data['addtime'] = time();
			
			$data['content'] = isset($_POST['data']['content'])?$_POST['data']['content']:'';
			$data['journey'] = isset($_POST['data']['journey'])?$_POST['data']['journey']:'';
			$data['money_say'] = isset($_POST['data']['money_say'])?$_POST['data']['money_say']:'';
			$data['disclaimer'] = isset($_POST['data']['disclaimer'])?$_POST['data']['disclaimer']:'';
			
			$tid = $this->do->add($data);
			if ($tid) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['diff'] = get_diff();
			$data['location'] = get_Cache('location');
			$data['group'] = get_Cache('tourism_tag');
			$data['goods'] = get_Cache('tourism_goods');
			$this->load->view ('admin/tourism/add',$data);
		}
	}
	
	public function edit() {
		
		if (is_ajax_request()) {
			$data = Posts('data');
			$data['gid'] = isset($data['gid'])?implode(',', $data['gid']):'';
			$data['content'] = isset($_POST['data']['content'])?$_POST['data']['content']:'';
			$data['journey'] = isset($_POST['data']['journey'])?$_POST['data']['journey']:'';
			$data['money_say'] = isset($_POST['data']['money_say'])?$_POST['data']['money_say']:'';
			$data['disclaimer'] = isset($_POST['data']['disclaimer'])?$_POST['data']['disclaimer']:'';
			
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['location'] = get_Cache('location');
			$data['diff'] = get_diff();
			$data['group'] = get_Cache('tourism_tag');
			$data['goods'] = get_Cache('tourism_goods');
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$this->load->view('admin/tourism/edit',$data);
		}
	}
	
	function sort(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$data = Posts('data');
			$result = $this->do->updates($data,array('id'=>$id));
			is_AjaxResult($result);
		}
	}
	
	function tj(){
		$id = Gets('id','checkid');
		$open = Gets('open','checkid');
		$result = $this->do->updates(array('tj'=>$open),array('id'=>$id));
		is_AjaxResult($result);
	}
	
	public function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
