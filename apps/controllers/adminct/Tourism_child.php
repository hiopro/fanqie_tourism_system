<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 旅游
 * @author chaituan@126.com
 */
class Tourism_child extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/TourismChild_model'=>'do','admin/Tourism_model'=>'do_tourism','admin/User_model'=>'do_user'));
	}
	
	public function index() {
		$this->load->view('admin/tourism_child/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"tourism.title like '%$name%'":'';
		
		$table = array('tourism'=>"tourism.id=tourism_child.tid+left");
		$data = $this->do->getItems_join($table,$where,'tourism_child.*,tourism.title','tourism_child.id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if(!isset($data['leader']))AjaxResult_error('请选择领队');
			$leader = implode(',', $data['leader']);
			$user = $this->do_user->getItems("id in ($leader)",'id,nickname,thumb');
			if(!$user)AjaxResult_error('领队数据错误');
			$data['leader'] = json_encode($user);
			$data['state']  = 1;
			$data['addtime'] = time();
			$data['sign_num'] = count($user);
			$tid = $this->do->add($data);
			if ($tid) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$id = Gets('id','num');
			if(!$id)showmessage('非法进入','error');
			$data['item'] = $this->do_tourism->getItem(array('id'=>$id),'in_line,full,day_num,line_num,id');
			$data['user'] = $this->do_user->getItems(array('leader'=>3,'gid'=>2),'id,nickname','id');
			$this->load->view ('admin/tourism_child/add',$data);
		}
	}
	
	public function edit() {
		
		if (is_ajax_request()) {
			$data = Posts('data');
			if(!isset($data['leader']))AjaxResult_error('请选择领队');
			$leader = implode(',', $data['leader']);
			$user = $this->do_user->getItems("id in ($leader)",'id,nickname,thumb');
			if(!$user)AjaxResult_error('领队数据错误');
			$data['leader'] = json_encode($user);
			$data['sign_num'] = count($user);
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$id = Gets('id','num');
			$data['item'] = $this->do->getItem(array('id'=>$id));
			$this->load->view('admin/tourism_child/edit',$data);
		}
	}
	
	function sort(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$sort = Posts('sort','num');
			$result = $this->do->updates(array('sort'=>$sort),array('id'=>$id));
			is_AjaxResult($result);
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
