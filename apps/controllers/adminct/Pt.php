<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 拼团
 * @author chaituan@126.com
 */
class Pt extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Pt_model'=>'do','admin/User_model'=>'do_user'));
	}
	
	public function index() {
		$this->load->view('admin/pt/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"title like '%$name%'":'';
		$data = $this->do->getItems($where,'','sort,id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request()) {
			$data = Posts('data');
			$data['tvid'] = implode(',', $data['tvid']);
			$data['gid'] = isset($data['gid'])?implode(',', $data['gid']):'';
			$data['addtime'] = time();
			
			$data['content'] = isset($_POST['data']['content'])?$_POST['data']['content']:'';
			$data['journey'] = isset($_POST['data']['journey'])?$_POST['data']['journey']:'';
			$data['money_say'] = isset($_POST['data']['money_say'])?$_POST['data']['money_say']:'';
			$data['disclaimer'] = isset($_POST['data']['disclaimer'])?$_POST['data']['disclaimer']:'';
			
			$user = $this->do_user->getItem(array('id'=>$data['leader_uid']),'nickname,thumb');
			$data['leader_nickname'] = $user['nickname'];
			$data['leader_thumb'] = $user['thumb'];
			$tid = $this->do->add($data);
			if ($tid) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['area'] = get_Cache('area');
			$data['ptarea'] = get_Cache('pt_area');
			$data['group'] = get_Cache('tourism_group');
			$data['venue'] = get_Cache('tourism_venue');
			$data['goods'] = get_Cache('tourism_goods');
			$data['user'] = $this->do_user->getItems(array('leader'=>3,'gid'=>2),'id,nickname');
			$this->load->view ('admin/pt/add',$data);
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			$data['tvid'] = implode(',', $data['tvid']);
			$data['gid'] = isset($data['gid'])?implode(',', $data['gid']):'';
			$data['addtime'] = time();
			
			$data['content'] = isset($_POST['data']['content'])?$_POST['data']['content']:'';
			$data['journey'] = isset($_POST['data']['journey'])?$_POST['data']['journey']:'';
			$data['money_say'] = isset($_POST['data']['money_say'])?$_POST['data']['money_say']:'';
			$data['disclaimer'] = isset($_POST['data']['disclaimer'])?$_POST['data']['disclaimer']:'';
			$user = $this->do_user->getItem(array('id'=>$data['leader_uid']),'nickname,thumb');
			$data['leader_nickname'] = $user['nickname'];
			$data['leader_thumb'] = $user['thumb'];
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['area'] = get_Cache('area');
			$data['ptarea'] = get_Cache('pt_area');
			$data['group'] = get_Cache('tourism_group');
			$data['venue'] = get_Cache('tourism_venue');
			$data['goods'] = get_Cache('tourism_goods');
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$data['user'] = $this->do_user->getItems(array('leader'=>3,'gid'=>2),'id,nickname');
			$this->load->view('admin/pt/edit',$data);
		}
	}
	
	function order(){
		$data['pid'] = Gets('pid');//拼团
		$data['dr_url'] ='/adminct/ptorder';
		$this->load->view('admin/ptorder/index',$data);
	}
	
	function sort(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$sort = Posts('sort','num');
			$result = $this->do->updates(array('sort'=>$sort),array('id'=>$id));
			is_AjaxResult($result);
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
