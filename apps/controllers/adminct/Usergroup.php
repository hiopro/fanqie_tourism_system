<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 后台首页
 * @author chaituan@126.com
 */
class Usergroup extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model('admin/UserGroup_model','do');
	}
	
	public function index() {
		$data ['items'] = $this->do->getItems ();
		$this->load->view('admin/usergroup/index', $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts ( 'data' );
			$data ['addtime'] = time ();
			$result = $this->do->add($data);
			if($result){
				$this->do->cache();
				AjaxResult_ok();
			}else{
				AjaxResult_error();
			}
		} else {
			$this->load->view('admin/usergroup/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$id = Posts('id','checkid');
			$result = $this->do->updates($data,"id=$id");
			if($result){
				$this->do->cache();
				AjaxResult_ok();
			}else{
				AjaxResult_error();
			}
		} else {
			$data ['item'] = $this->do->getItem ( "id=" . Gets ( 'id', 'checkid' ) );
			$this->load->view ( 'admin/usergroup/edit', $data );
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		$result = $this->do->deletes(array('id' => $id));
		if($result){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
