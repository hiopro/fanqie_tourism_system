<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 后台首页
 * @author chaituan@126.com
 */
class Manager extends AdminCommon {
	
	public function index() {
		$this->load->model('admin/User_model','do_user');
		
		$data['sql_version'] = $this->AdminMenu_model->version();
		$t = array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
		//获得前一天的时间
		$items = $this->do_user->getItems("FROM_UNIXTIME(addtime,'%Y-%m-%d')=DATE_FORMAT(date_sub(curdate(),interval 1 day),'%Y-%m-%d')","FROM_UNIXTIME(addtime,'%k') as h,id",'addtime');
		if($items){
			foreach ($items as $k=>$v){
				if(in_array($v['h'], $t)){
					$new[$v['h']][] = $k;
				}
			}
			foreach ($new as $v){$zst[] = count($v);}
		}else{
			$zst = [0];
		}
		
		$data['zst'] = json_encode($zst);
		$data['sql_version'] = $this->AdminMenu_model->version();
		$this->load->view ('admin/index', $data);
	}
	
	public function config() {
		$data = get_Cache('admin_config');
		foreach ( $data as $v ) {
			if ($v ['tkey'] == 'system') {
				$datas ['items'][] = $v;
			}
		}
		$this->load->view ('admin/config/views', $datas);
	}
	
	public function password() {
		if(is_ajax_request()){
			$oldpass = Posts('oldpass');
			$item = $this->AdminUser_model->getItem(array('id'=>$this->loginUser['id']),'password,encrypt');
			$pwd = get_password($oldpass, $item['encrypt']);
			if($pwd != $item['password'])AjaxResult_error('原始密码不正确');
			$new = set_password(Posts('password'));
			$result = $this->AdminUser_model->updates(array('password'=>$new['password'],'encrypt'=>$new['encrypt']),"id=".$this->loginUser['id']);
			is_AjaxResult($result);
		}
	}
}
