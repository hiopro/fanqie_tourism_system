<?php
use phpDocumentor\Reflection\Types\This;
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 游记
 * @author chaituan@126.com
 */
class Unews extends CI_Controller {
	
	function cmt(){
		if(is_ajax_request()){
			$id = Posts('id');
			$say = Posts('say');
			$uid = Posts('uid');
			$this->load->model(array('admin/UserNewsCmt_model','admin/User_model'));
			$user = $this->User_model->getItem(array('id'=>$uid),'nickname,thumb');
			$data = array('unid'=>$id,'uid'=>$uid,'nickname'=>$user['nickname'],'thumb'=>$user['thumb'],'says'=>$say,'addtime'=>time());
			$result = $this->UserNewsCmt_model->add($data);
			is_AjaxResult($result);
		}
	}
	
	function cmt_list(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$this->load->model(array('admin/UserNewsCmt_model','admin/UserNews_model'));
			$result = $this->UserNewsCmt_model->getItems(array('unid'=>$id));
			$cmt = $this->UserNewsCmt_model->count(array('unid'=>$id));
			$zan = $this->UserNews_model->getItem(array('id'=>$id),'zan');
			$data = array('items'=>$result,'zan'=>intval($zan['zan']),'cmt'=>$cmt);
			if($data){
				AjaxResult(1, '添加成功',$data);
			}else{
				AjaxResult_error('没有数据');
			}
		}
	}
	
	function cmt_zan(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$uid = Posts('uid','num');
			$this->load->model(array('admin/UserNews_model','admin/Star_model'));
			$item = $this->Star_model->getItem(array('uid'=>$uid,'tid'=>$id,'cid'=>1));
			if($item)AjaxResult_error('您已经点过赞了');
			$this->UserNews_model->updates(array('zan'=>'+=1'),array('id'=>$id));
			$result = $this->Star_model->add(array('uid'=>$uid,'tid'=>$id,'cid'=>1));
			is_AjaxResult($result,'点赞成功');
		}
	}
	
	function upload(){
		$path = "/res/upload/images/{yyyy}{mm}{dd}/{time}{rand:6}";
		$config = array ("pathFormat" => $path,"maxSize" => 10000, "allowFiles" => array(".gif",".png",".jpg",".jpeg",".ogg",'.mp4','.webm','.mp3','.wav'));
		$this->load->library('Uploader', array ('fileField' =>'file','config' => $config));
		$info = $this->uploader->getFileInfo();
		if ($info ['state'] == 'SUCCESS') {
			AjaxResult(1, '添加成功',array('url'=>$info['url']));
		} else {
			AjaxResult_error($info['state']);
		}
	}
}
